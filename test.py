from keras.models import load_model
import numpy as np

def predict():
    model = load_model('final_three_lstm1.h5')
    with open('dataset.txt', 'r', encoding='utf-8') as f:
        data = f.read().lower()
    x = " "
    maxlen = 45
    chars = sorted(list(set(data)))
    char_indices = dict((char, chars.index(char)) for char in chars)
    start_idx = np.random.randint(0, len(data) - maxlen - 1)
    new_sonnet = data[start_idx:start_idx + maxlen]
    # print(new_sonnet)
    for i in range(600):
        # Vectorize generated text
        sampled = np.zeros((1, maxlen, len(chars)))
        for j, char in enumerate(new_sonnet):
            sampled[0, j, char_indices[char]] = 1
        preds = model.predict(sampled, verbose=0)[0]
        preds = np.asarray(preds).astype('float64')
        preds = np.log(preds) / 0.5
        exp_preds = np.exp(preds)
        preds = exp_preds / np.sum(exp_preds)
        probas = np.random.multinomial(1, preds, 1)
        pred_idx = np.argmax(probas)

        next_char = chars[pred_idx]

        # Append predicted character to seed text
        new_sonnet += next_char
        new_sonnet = new_sonnet[1:]
        x += next_char

    print(x)

predict()